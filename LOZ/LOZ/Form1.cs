﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOZ.Classes;

namespace LOZ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            asignar();
        }
        Heroe Link = new Heroe();
        Armas armas = new Armas();
        Rocas rocas = new Rocas();
        Enemigos enemigos = new Enemigos();
        ControlJuego control = new ControlJuego();

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Link.mover(sender, e, Width, Height);
            Link.checarColicion(armas, rocas, enemigos);
            control.actualizarLabels(label1, label2, label3);
            control.Ganar(panel1);
            Link.actualizarPos();
        }

        public void asignar()
        {
            Link.Imagen = pictureBox2;
            control.Jugador = Link;
            armas.agregar(pictureBox3);
            rocas.agregar(pictureBox4);
            rocas.agregar(pictureBox5);
            rocas.agregar(pictureBox6);
            rocas.agregar(pictureBox7);
            rocas.agregar(pictureBox8);
            enemigos.agregar(pictureBox9);
            enemigos.agregar(pictureBox10);
        }

        private void pictureBox2_EnabledChanged(object sender, EventArgs e)
        {
            Close();
        }
    }
}
