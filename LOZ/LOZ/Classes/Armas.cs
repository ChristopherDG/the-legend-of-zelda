﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOZ.Classes
{
    class Armas
    {
        List<PictureBox> listaArmas;

        public Armas()
        {
            this.ListaArmas = new List<PictureBox>();
        }

        public List<PictureBox> ListaArmas { get => listaArmas; set => listaArmas = value; }

        public void agregar(PictureBox imagen)
        {
            ListaArmas.Add(imagen);
        }
    }
}
