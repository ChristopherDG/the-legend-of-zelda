﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOZ.Classes
{
    class Rocas
    {
        List<PictureBox> listaRocas;

        public Rocas()
        {
            this.ListaRocas = new List<PictureBox>();
        }

        public List<PictureBox> ListaRocas { get => listaRocas; set => listaRocas = value; }

        public void agregar(PictureBox imagen)
        {
            ListaRocas.Add(imagen);
        }
    }
}
