﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOZ.Classes
{
    class Enemigos
    {
        List<PictureBox> listaEnemigos;

        public Enemigos()
        {
            this.ListaEnemigos = new List<PictureBox>();
        }

        public List<PictureBox> ListaEnemigos { get => listaEnemigos; set => listaEnemigos = value; }

        public void agregar(PictureBox imagen)
        {
            ListaEnemigos.Add(imagen);
        }
    }
}
