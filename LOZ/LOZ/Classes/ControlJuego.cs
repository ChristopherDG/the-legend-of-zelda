﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOZ.Classes
{
    class ControlJuego
    {
        Heroe jugador;

        public ControlJuego()
        {
            this.Jugador = null;
        }

        internal Heroe Jugador { get => jugador; set => jugador = value; }

        public void actualizarLabels(Label label1, Label label2, Label label3)
        {
            if (Jugador.Equipada)
            {
                label1.Text = "Arma Equipada: Espada Maestra";
                label2.Text = "Mision: Elimina a todos los enemigos";
            }

            if (Jugador.Puntaje != 0)
            {
                if (Jugador.Puntaje == 20)
                {
                    label2.Text = "Mision: Ve a la salida";
                }
                label3.Text = "Puntaje: " + Jugador.Puntaje.ToString();
            }
        }

        public void Ganar(Panel panel)
        {
            if(jugador.Puntaje == 20)
            {
                panel.Top = 331;
                panel.Visible = true;
            }
            if (jugador.Imagen.Bounds.IntersectsWith(panel.Bounds))
            {
                jugador.Imagen.Top = Jugador.Y;
                jugador.Imagen.Left = jugador.X;
                MessageBox.Show("Felicidades haz completado el nivel");
                jugador.Imagen.Enabled = false;
            }
        }
    }
}
