﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LOZ.Classes
{
    class Heroe
    {
        PictureBox imagen;
        bool equipada;
        int x, y;
        int puntaje;

        public Heroe()
        {
            this.Imagen = null;
            this.Equipada = false;
            this.Puntaje = 0;
        }

        public PictureBox Imagen { get => imagen; set => imagen = value; }
        public bool Equipada { get => equipada; set => equipada = value; }
        public int Puntaje { get => puntaje; set => puntaje = value; }
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }

        public void mover(object sender, KeyEventArgs e, int width, int height)
        {
            if(e.KeyCode == Keys.Up)
            {
                if((this.Y) > 0)
                {
                    Imagen.Top -= 30;
                }
            }else if(e.KeyCode == Keys.Down)
            {
                if((this.Y)+90 < height)
                {
                    imagen.Top += 30;
                }
            }else if(e.KeyCode == Keys.Left)
            {
                if((this.X) > 0)
                {
                    imagen.Left -= 30;
                }
            }else if(e.KeyCode == Keys.Right)
            {
                if ((this.X) + 60 < width)
                {
                    imagen.Left += 30;
                }
            }
        }

        public void actualizarPos()
        {
            this.X = imagen.Location.X;
            this.Y = imagen.Location.Y;
        }

        public void checarColicion(Armas armas, Rocas rocas, Enemigos enemigos)
        {
            foreach (PictureBox arma in armas.ListaArmas)
            {
                if (this.Imagen.Bounds.IntersectsWith(arma.Bounds))
                {
                    Equipada = true;
                    arma.Visible = false;
                    arma.Enabled = false;
                }
            }

            foreach (PictureBox roca in rocas.ListaRocas)
            {
                if (this.imagen.Bounds.IntersectsWith(roca.Bounds))
                {
                    this.imagen.Top = this.Y;
                    this.imagen.Left = this.X;
                }
            }

            foreach (PictureBox enemigo in enemigos.ListaEnemigos)
            {
                if (this.imagen.Bounds.IntersectsWith(enemigo.Bounds))
                {
                    if (this.Equipada)
                    {
                        enemigo.Visible = false;
                        enemigo.Top = 9999;
                        this.Puntaje += 10;
                    }
                    else
                    {
                        this.imagen.Top = this.Y;
                        this.imagen.Left = this.X;
                    }
                }
            }
        }
    }
}
